stages:
  - build
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always

.auth: &auth
  - |
    if [[ -z "$CI_MERGE_REQUEST" ]] && ([[ $CI_COMMIT_BRANCH == "master" ]] || [[ $CI_COMMIT_BRANCH == "dev" ]]); then
      GPG_TTY=$(tty) gpg --batch --yes --pinentry-mode loopback --passphrase "$GPG_PASSPHRASE" -d ~/.password-store/docker-credential-helpers/docker-pass-initialized-check.gpg
      pass show docker-credential-helpers/docker-pass-initialized-check
    fi

merge_request:
  image: docker.io/docker:23.0.1-git
  stage: build
  allow_failure: false
  only:
    - merge_requests
  script:
    - apk add --no-cache openssl
    - docker buildx use mybuilder
    - password_len=20
    - echo -n "$(openssl rand -base64 $password_len | tr -dc 'a-zA-Z0-9' | fold -w $password_len | head -n 1)" > gpg_password.txt
    - >
      docker buildx build .
      --secret id=gpg_password,src=gpg_password.txt
      --progress plain
      --platform=linux/amd64
  tags:
    - cryptexlabs-public-docker

build:
  image: cryptexlabs/docker-git:2.2.8
  stage: build
  allow_failure: false
  rules:
    - if: $CI_MERGE_REQUEST
      when: never
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "master"
      when: always
  before_script:
    - *auth
  script:
    - docker buildx use mybuilder
    - password_len=20
    - echo -n "$(openssl rand -base64 $password_len | tr -dc 'a-zA-Z0-9' | fold -w $password_len | head -n 1)" > gpg_password.txt
    - >
      docker buildx build .
      --secret id=gpg_password,src=gpg_password.txt
      --progress plain
      --platform=linux/amd64
      --tag $CI_REGISTRY_IMAGE:commit-$CI_COMMIT_SHORT_SHA
      --push
  tags:
    - cryptexlabs-org-docker

build-unprotected:
  image: docker.io/docker:23.0.1-git
  stage: build
  allow_failure: false
  only:
    - branches
  except:
    - master
    - dev
  script:
    - apk add --no-cache openssl
    - docker buildx use mybuilder
    - password_len=20
    - echo -n "$(openssl rand -base64 $password_len | tr -dc 'a-zA-Z0-9' | fold -w $password_len | head -n 1)" > gpg_password.txt
    - >
      docker buildx build .
      --secret id=gpg_password,src=gpg_password.txt
      --progress plain
      --platform=linux/amd64
      --tag $CI_REGISTRY_IMAGE:commit-$CI_COMMIT_SHORT_SHA
  tags:
    - cryptexlabs-public-docker

release:
  image: cryptexlabs/docker-git:2.2.8
  stage: release
  rules:
    - if: $CI_MERGE_REQUEST
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
  before_script:
    - *auth
  needs:
    - build
  script:
    - version=$(cat version)
    - docker pull $CI_REGISTRY_IMAGE:commit-$CI_COMMIT_SHORT_SHA
    - docker tag $CI_REGISTRY_IMAGE:commit-$CI_COMMIT_SHORT_SHA $DOCKER_IO_REGISTRY_IMAGE:latest
    - docker tag $CI_REGISTRY_IMAGE:commit-$CI_COMMIT_SHORT_SHA $DOCKER_IO_REGISTRY_IMAGE:$version
    - docker tag $CI_REGISTRY_IMAGE:commit-$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:latest
    - docker push $DOCKER_IO_REGISTRY_IMAGE:latest
    - docker push $DOCKER_IO_REGISTRY_IMAGE:$version
    - docker push $CI_REGISTRY_IMAGE:latest
    - |
        echo "$GIT_SSH_PK" | base64 -d > id_rsa;
        chmod 400 id_rsa;
        echo 'ssh -i ./id_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $*' > ssh;
        chmod +x ssh;
        git config --global user.name "$GIT_USER_NAME";
        git config --global user.email "$GIT_USER_EMAIL";
        git tag -d $version || true;
        git tag $version;
        git remote set-url origin git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git;
        GIT_SSH='./ssh' git push origin $version;
  tags:
    - cryptexlabs-org-docker