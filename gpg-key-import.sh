echo -n "$GPG_PRIVATE_KEY" | base64 -d > private.pgp
echo -n "$GPG_PASSPHRASE" | gpg --batch --yes --import private.pgp
gpg --list-keys --fingerprint --with-colons | sed -E -n -e 's/^fpr:::::::::([0-9A-F]+):$/\1:6:/p' | gpg --import-ownertrust
rm -r .password-store/
key_id=$(gpg --list-keys "$GPG_USER_NAME" | grep -B 1 "$GPG_USER_NAME" | grep -v "$GPG_USER_NAME" | awk '{print $1}')
pass init "$key_id"
password_len=20
pass_password="$(openssl rand -base64 $password_len | tr -dc 'a-zA-Z0-9' | fold -w $password_len | head -n 1)"
echo -n "$pass_password" | PASSWORD_STORE_X_SELECTION=primary pass insert --force --multiline docker-credential-helpers/docker-pass-initialized-check
GPG_TTY=$(tty) gpg --batch --yes --pinentry-mode loopback --passphrase "$GPG_PASSPHRASE" -d ~/.password-store/docker-credential-helpers/docker-pass-initialized-check.gpg
pass show docker-credential-helpers/docker-pass-initialized-check