FROM cryptexlabs/docker-git:2.2.8
ARG KUBERNETES_VERSION="v1.25.2"
ARG HELM_VERSION="v3.3.4"
ARG HELMFILE_VERSION="v0.154.3-dirty"
ARG HELM_SECRETS_VERSION="v3.8.2"
ARG SOPS_VERSION="v3.7.3"
ARG TERRAFORM_VERSION="1.4.5"
ARG TFSEC_VERSION="1.28.1"

ARG ARCH_CMD='eval uname -m | grep -q x86_64 && echo -n "amd64" || echo -n "arm64"'

#Installs latest Chromium package.
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.17/main" >> /etc/apk/repositories \
    && apk upgrade -a

RUN apk update \
  && apk --no-cache add \
  bash \
  curl \
  git \
  jq \
  nodejs \
  npm \
  openssh \
  py-pip \
  libtool \
  build-base \
  make \
  autoconf \
  automake \
  shadow \
  openssl \
  ca-certificates \
  libstdc++ \
  harfbuzz \
  nss \
  freetype \
  ttf-freefont \
  wqy-zenhei \
  libffi-dev \
  python3-dev \
  chromium \
  tzdata \
  grep

ENV LD_LIBRARY_PATH=/usr/local/lib
ENV OPENSSL_ROOT_DIR=/usr/local/opt/openssl

RUN apk update && apk add --no-cache \
    perl-utils \
    zip

RUN apk upgrade

RUN apk update && apk add --no-cache cmake

RUN apk update && apk add --no-cache boost-dev ffmpeg-dev libressl-dev git-lfs

RUN git clone https://github.com/google/googletest.git \
    && cd googletest \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make \
    && make install \
    && cd .. \
    && cd .. \
    && rm -rf googletest

RUN ln -sf $(which python3) /usr/bin/python

# kubernetes helm
# Note: Latest version of helm may be found at
# https://github.com/kubernetes/helm/releases

RUN curl -k -L "https://dl.k8s.io/release/$KUBERNETES_VERSION/bin/linux/$($ARCH_CMD)/kubectl" -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && chmod g+rwx /root \
    && mkdir /config \
    && chmod g+rwx /config

# Helmfile
RUN curl -fsSL -o helmfile https://github.com/woodcockjosh/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_linux_$($ARCH_CMD) \
    && chmod +x helmfile \
    && mv helmfile /usr/local/bin/helmfile \
    && helmfile --version

# SOPS
RUN curl -fsSL -o sops https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux.$($ARCH_CMD) \
    && chmod +x sops \
    && mv sops  /usr/local/bin/sops

# Yq
RUN wget -q -O /usr/bin/yq $(wget -q -O - https://api.github.com/repos/mikefarah/yq/releases/latest | \
    jq -r ".assets[] | select(.name == \"yq_linux_$($ARCH_CMD)\") | .browser_download_url") \
    && chmod +x /usr/bin/yq

# dyff
RUN curl -fsSL  --location https://raw.githubusercontent.com/homeport/dyff/main/scripts/download-latest.sh | bash

# Terraform , tflint, tfsec , terragrunt
RUN curl -fsSL -o terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_$($ARCH_CMD).zip \
    && unzip  -d /usr/bin/ terraform.zip && rm terraform.zip

RUN curl -fsSL "https://github.com/terraform-linters/tflint/releases/latest/download/tflint_linux_$($ARCH_CMD).zip" -o "/usr/local/bin/tflint_linux_$($ARCH_CMD).zip"
RUN unzip /usr/local/bin/tflint_linux_$($ARCH_CMD).zip -d /usr/local/bin && rm -f "/usr/local/bin/tflint_linux_$($ARCH_CMD).zip"

RUN curl -fsSL "https://github.com/aquasecurity/tfsec/releases/download/v${TFSEC_VERSION}/tfsec_${TFSEC_VERSION}_linux_$($ARCH_CMD).tar.gz" -o "/usr/local/bin/tfsec_${TFSEC_VERSION}_linux_$($ARCH_CMD).tar.gz"

RUN tar -xzvf /usr/local/bin/tfsec_${TFSEC_VERSION}_linux_$($ARCH_CMD).tar.gz tfsec-checkgen tfsec -C  /usr/local/bin && chmod 755 /usr/local/bin/* \
    && rm -f /usr/local/bin/tfsec_${TFSEC_VERSION}_linux_$($ARCH_CMD).tar.gz

RUN curl -fsSL "https://github.com/gruntwork-io/terragrunt/releases/latest/download/terragrunt_linux_$($ARCH_CMD)" -o "/usr/local/bin/terragrunt_linux_$($ARCH_CMD)"
RUN mv /usr/local/bin/terragrunt_linux_$($ARCH_CMD) /usr/local/bin/terragrunt && chmod 755 /usr/local/bin/terragrunt

# Installing PosgreSQL
RUN apk --update add postgresql-client

RUN rm -rf /var/cache/* \
    && mkdir /var/cache/apk

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

# Nodejs setup
RUN addgroup --gid 500 -S node && adduser --uid 500 -S node -G node -h /home/node -s /bin/bash
RUN mkdir -p /home/node/.npm-global && chown node:node /home/node/.npm-global
RUN mkdir -p /home/node/.docker/buildx && chown -R node:node /home/node/.docker
ENV HOME=/home/node
ENV USER=node
WORKDIR $HOME
RUN mkdir -p $HOME/.config && chown -R node:node $HOME/.config
ENV PATH=$HOME/.npm-global/bin:$HOME/.local/bin:$PATH
ENV NPM_CONFIG_PREFIX=$HOME/.npm-global
RUN chmod -R u+w $HOME/.npm-global

# GPG Script
COPY gpg-key-import.sh /usr/bin/
RUN chmod 555 /usr/bin/gpg-key-import.sh
COPY gpg_file.txt .
RUN chown node:node gpg_file.txt

# Docker secure store setup
USER node
COPY config.json $HOME/.docker/
RUN mkdir -p $HOME/.gnupg/ \
    && chmod 700 ~/.gnupg
RUN --mount=type=secret,id=gpg_password,uid=500 cat /run/secrets/gpg_password > /dev/null
RUN --mount=type=secret,id=gpg_password,uid=500 cat gpg_file.txt | sed 's/gpg_password/'"`cat /run/secrets/gpg_password`"'/g' | gpg --batch --generate-key
RUN pass init $(gpg --list-secret-keys dockertester@docker.com | sed -n '/sec/{n;p}' | sed 's/^[[:space:]]*//g')

# Pip
RUN python3 -m venv $HOME/venv \
    && source $HOME/venv/bin/activate

RUN pip install --upgrade pip --break-system-packages

RUN pip install \
    anybadge \
    awscli \
    yamlpath \
    checkov \
    json-spec \
    --break-system-packages

# NPM
RUN npm install -g npm@latest
RUN npm i -g \
typescript \
yarn \
plagiarize@0.0.55 \
expo-cli \
serverless

# Update node gyp
USER root
RUN apk upgrade \
    && apk add vips-dev \
    && (find /usr/lib -type d -name "node-gyp" -exec sh -c 'cd "$(dirname "{}")" && npm i node-gyp@latest --save' \; || true) \
    && find /usr/lib -type d -name "node-gyp" -exec sh -c 'cd "$(dirname "{}")" && npm i node-gyp@latest --save' \;
USER node

RUN helm plugin install https://github.com/databus23/helm-diff \
    && helm plugin install https://github.com/jkroepke/helm-secrets --version ${HELM_SECRETS_VERSION}


USER root

# Install go
ENV GOPATH=/go \
    GOROOT=/usr/lib/go \
    PATH=/go/bin:$PATH

RUN mkdir -p "${GOPATH}/src" "${GOPATH}/bin" \
    && chown -R node:node $GOPATH



RUN apk add --no-cache \
    musl-dev \
    go \
    && rm -rf /var/cache/* \
    && mkdir /var/cache/apk

# Install ko
RUN echo https://dl-cdn.alpinelinux.org/alpine/edge/testing/ >> /etc/apk/repositories \
    && apk update \
    && apk add ko

# create gitlab runner user and add it to node group
# Note: On older versions of gitlab-runner uid 999 was used so if you use this image you will have to update your gitlab runner
RUN groupadd -g 997 ci-group-1 || true
RUN groupadd -g 998 ci-group-2 || true
RUN groupadd -g 999 ci-group-3 || true
RUN usermod -aG 997 node
RUN usermod -aG 998 node
RUN usermod -aG 999 node

RUN git config --global --add safe.directory '*'

USER node

# Validation
RUN  (                                                        echo "==========================================" \
    && echo "Node       " && node       --version          && echo "==========================================" \
    && echo "NPM        " && npm        --version          && echo "==========================================" \
    && echo "YARN       " && yarn       --version          && echo "==========================================" \
    && echo "Python     " && python     --version          && echo "==========================================" \
    && echo "Python  PIP" && python      -m pip list       && echo "==========================================" \
    && echo "Kubectl    " && kubectl      version --client && echo "==========================================" \
    && echo "Helm       " && helm         version --short  && echo "==========================================" \
    && echo "Helm plugin" && helm         plugin list      && echo "==========================================" \
    && echo "helmfile   " && helmfile   --version          && echo "==========================================" \
    && echo "SOPS  PIP  " && sops       --version          && echo "==========================================" \
    && echo "Terraform  " && terraform  --version          && echo "==========================================" \
    && echo "Terragrunt " && terragrunt --version          && echo "==========================================" \
    && echo "Tflint     " && tflint     --version          && echo "==========================================" \
    && echo "YQ         " && yq         --version          && echo "==========================================" \
    && echo "DYFF       " && dyff         version          && echo "==========================================" \
    && echo "Checkov    " && checkov    --version          && echo "==========================================" \
    && echo "Go         " && go         version            && echo "==========================================" \
)


SHELL ["/bin/bash", "-c"]
